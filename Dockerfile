FROM composer:2.0 as composer

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist \
    --no-dev \
    --optimize-autoloader

FROM php:8.0.8-fpm as fpm-prod

#RUN apt-get install -y php-pgsql
RUN apt-get update && \
    apt-get install -y --no-install-recommends libssl-dev zlib1g-dev curl git unzip libxml2-dev libpq-dev libzip-dev && \
    pecl install apcu && \
    docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql && \
    docker-php-ext-install -j$(nproc) zip opcache intl pdo_pgsql pgsql sockets && \
    docker-php-ext-enable apcu pdo_pgsql sodium && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY . .
COPY ./docker/php-fpm/php.ini /usr/local/etc/php/conf.d/downloader-php.ini
#COPY ./docker/php-fpm/php-fpm.conf /usr/local/etc/php-fpm.d/downloader-docker.conf

#RUN chmod -R 777 storage
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY --from=composer /app/vendor/ /apps/downloader/vendor

FROM fpm-prod as fpm-dev

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip unzip
