<?php

namespace App\Services;

use App\Exceptions\UnableToSaveFileException;
use App\Jobs\UpdateFileDownloadCount;
use Illuminate\Support\Facades\Storage;
use App\Models\File;


class FileService
{
    public function download(File $file, ?string $userId = null)
    {
        $response = Storage::disk('s3')->download($file->path);

        UpdateFileDownloadCount::dispatch($file->uuid, $userId);

        return $response;
    }
}
