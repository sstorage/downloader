<?php

namespace App\Services;

use Tymon\JWTAuth\JWT;

class JwtService
{
    protected $jwt;

    public function __construct(JWT $jwt)
    {
        $this->jwt = $jwt;
        try {
            $jwt->setToken($jwt->getToken());
        } catch (\Exception $e) {

        }
    }

    public function getUserId()
    {
        try {
            return $this->jwt->getClaim('sub');
        } catch (\Exception $e) {
            return null;
        }
    }
}
