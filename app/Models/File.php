<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\File
 *
 * @property string $uuid
 * @property string $name
 * @property string $mime_type
 * @property string $path
 * @property int $size
 */
class File extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $keyType = 'string';
    protected $primaryKey = 'uuid';

    protected $hidden = ['path'];

    protected $fillable = [
        'uuid',
        'name',
        'mime_type',
        'path',
        'size'
    ];
}
