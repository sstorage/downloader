<?php

namespace App\Http\Resources;

use App\Http\Responses\PaginatedApiResourceResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\PaginatedResourceResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ApiCollectionResource extends ResourceCollection
{
    public $success = true;
    public $async = false;

    protected $notifications = [];

    /**
     * Customize the response for a request.
     *
     * @param  Request  $request
     * @param  JsonResponse  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $responseData = $response->getData(true);

        $this->changeDataStructure($responseData);

        $response->setData($responseData);
    }

    /**
     * Create a paginate-aware HTTP response.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    protected function preparePaginatedResponse($request)
    {
        if ($this->preserveAllQueryParameters) {
            $this->resource->appends($request->query());
        } elseif (! is_null($this->queryParameters)) {
            $this->resource->appends($this->queryParameters);
        }
        $this->additional['notifications'] = $this->notifications;
        $response = $this->getPaginatedResourceResponse()->toResponse($request);
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        return $response;
    }

    protected function getPaginatedResourceResponse(): PaginatedResourceResponse
    {
        $response = new PaginatedApiResourceResponse($this);
        $response->success = $this->success;
        $response->async = $this->async;
        return $response;
    }

    private function changeDataStructure(array &$requestData): void
    {
        $data=$requestData['data'];
        unset($requestData['data']);
        $requestData['data'] = ['items'=>$data];
        ksort($requestData);
    }
}
