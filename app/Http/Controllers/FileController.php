<?php

namespace App\Http\Controllers;

use App\Exceptions\UnableToSaveFileException;
use App\Http\Resources\ApiCollectionResource;
use App\Models\File;
use App\Services\FileService;
use App\Services\JwtService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FileController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @throws UnableToSaveFileException
     */
    public function index(Request $request): ApiCollectionResource
    {
        return new ApiCollectionResource(File::paginate($request->get('per_page')));
    }

    public function download(File $file, FileService $fileService, JwtService $jwt)
    {
        return $fileService->download($file, $jwt->getUserId());
    }
}
